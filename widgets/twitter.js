class TwitterWidget extends Widget {

    constructor(name) {
        super();
        if (name === undefined)
            this.account_name = "MichelBillaud";
        else
            this.account_name = name;
    }

    apply_to(parent) {

        parent.append("<div class='panel panel-primary'>" +
            "<div class='panel-heading'>Twitter timeline" +
            "<button class='fa fa-times delete_cross' aria-hidden='true'></button></div>" +
            "<div class='panel-body twitter_widget'>" +
            "<div class='container'>" +
            "<div class='row'>" +
            "<div class='twitter-search'>" +
            "<input type='text' class='form-control input-sm twitter-input' maxlength='64' placeholder='Search' />" +
            "<button type='button' class='twitter-btn btn btn-primary btn-sm'>Search</button>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "<div class='timeline-div'></div>" +
            "<a class='twitter-timeline'" +
            " href='https://twitter.com/" + this.account_name + "'>" +
            "Tweets by @" + this.account_name + " </a>" +
            "<script async src='https://platform.twitter.com/widgets.js' charset='utf-8'></script>" +
            "</div>" +
            "</div>");

        $(".twitter-btn").click(function () {
            let this_widget = $(this).parentsUntil(".panel").parent();
            if ($(this_widget).find(".twitter-input").val()) {
                $(this_widget).find(".twitter-timeline").remove();
                $(this_widget.find(".timeline-div")).replaceWith("<div class='timeline-div'></div>" +
                    "<a class='twitter-timeline'" +
                    " href='https://twitter.com/" + $(this_widget).find(".twitter-input").val() + "'>" +
                    "Tweets by @" + $(this_widget).find(".twitter-input").val() + " </a>" +
                    "<script async src='https://platform.twitter.com/widgets.js' charset='utf-8'></script>" +
                    "</div>");
                $(this_widget).find(".twitter-input").val("");
            }
        });

        $(".twitter-input").keypress(function (e) {
            if (e.keyCode == 13) {
                $(".twitter-btn").trigger("click");
            }
        });
    }

    actualise() {
        let ac_name = this.account_name;
        window.setInterval(function () {

            let this_widget = $(this).parentsUntil(".panel").parent();
            $(this_widget.find(".timeline-div")).replaceWith("<div class='timeline-div'></div>" +
                "<a class='twitter-timeline'" +
                " href='https://twitter.com/" + ac_name + "'>" +
                "Tweets by @" + ac_name + " </a>" +
                "<script async src='https://platform.twitter.com/widgets.js' charset='utf-8'></script>" +
                "</div>");
        }, 30000);
    }
}