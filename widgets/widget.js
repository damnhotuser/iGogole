// abstract class
class Widget {

    // abstract method
    constructor() {

    }

    // abstract method
    apply_to(parent) {
        let widget_colors = ["lightsteelblue", "darksalmon", "darkgoldenrod",
            "cadetblue", "tomato", "forestgreen", "lightcoral"];

        parent.innerHTML = "<span class='fa fa-mouse-pointer configure-widget-icon'></span>" +
            "<p class='configure-widget-label'>Choose your widget from sidebar!</p>";

        parent.setAttribute("background-color", widget_colors[Math.floor((Math.random() * 7) + 1) - 1]);
    }

    // abstract method
    actualise(){
        throw new Error("abstract method, you have to implement it.");
    }
}

