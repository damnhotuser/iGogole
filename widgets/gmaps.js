/**
 * Created by damnhotuser on 16/03/17.
 */

class GMapsWidget extends Widget {

    constructor() {
        super();
    }

    apply_to(parent) {

        const iutlatlng = new google.maps.LatLng(44.791586, -0.608818);
        const options = {
            center: iutlatlng,
            zoom: 17,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true,
            draggable: false,
        };

        parent.append("<div class='panel panel-primary'>" +
            "<div class='panel-heading'>Google Map" +
            "<button class='fa fa-times delete_cross' aria-hidden='true'></button></div>" +
            "<div class='panel-body gmaps_widget'>" +
            "<div class='row gmaps-search'>" +
            "<input type='text' class='form-control input-sm gmaps-input' maxlength='64' placeholder='Search' />" +
            "<button type='button' class='gmaps-btn btn btn-primary btn-sm'>Search</button>" +
            "</div>" +
            "<div class='gmaps'></div>" +
            "</div>" +
            "</div>"
        )
        ;

        $('.gmaps').load(function () {
            const carte = new google.maps.Map($(this).get(0), options);
        }).trigger("load");

        //http://maps.google.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA

        $('.gmaps-btn').click(function () {
            let this_widget = $(this).parentsUntil(".panel").parent();
            if($(this_widget).find('.gmaps-input').val()) {
                $.ajax({
                    url: "https://maps.google.com/maps/api/geocode/json?key=AIzaSyAQ04n2aMZg4DLMP5WwOl9lvqJiS2IdPAU&address="+$(this_widget).find('.gmaps-input').val(),
                    type: 'get',
                    async: false,
                    dataType: 'json',
                    success: function (data) {
                        var directionsDisplay = new google.maps.DirectionsRenderer;
                        var directionsService = new google.maps.DirectionsService;

                        const lat = data.results[0].geometry.location.lat;
                        const lng = data.results[0].geometry.location.lng;
                        const deplatlng = new google.maps.LatLng(lat, lng);

                        const destlatlng = new google.maps.LatLng(44.791586, -0.608818);

                        const options = {
                            zoom: 14,
                            center: destlatlng,
                            disableDefaultUI: true,
                            draggable: false,
                        };

                        const carte = new google.maps.Map($('.gmaps').get(0), options);

                        directionsDisplay.setMap(carte);

                        directionsService.route({
                            origin      : deplatlng,
                            destination : destlatlng,
                            travelMode: google.maps.TravelMode.DRIVING
                        }, function(response, status) {
                            if (status == google.maps.DirectionsStatus.OK) {
                                directionsDisplay.setDirections(response);
                            } else {
                                window.alert('Directions request failed due to ' + status);
                            }
                        });
                    }
                });
            }
        });

        $(".gmaps-input").keypress(function (e) {
            if (e.keyCode == 13) {
                $(".gmaps-btn").trigger("click");
            }
        });
    }
}