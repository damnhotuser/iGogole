
class WeatherWidget extends Widget {

    //"http://api.worldweatheronline.com/premium/v1/weather.ashx?key=5087dcc1c04f4b40ad1200824171503&format=json&&fx=no&mca=no&show_comments=no&q=Bordeaux"

    constructor() {
        super();
    }

    apply_to(parent) {

        parent.append("<div class='panel panel-primary'>" +
            "<div class='panel-heading'>Weather" +
            "<button class='fa fa-times delete_cross' aria-hidden='true'></button></div>" +
            "<div class='panel-body weather_widget'>" +
            "<h3 class='weather_search_label'>search for a city's weather...</h3> " +
            "<div class='weather-search'>" +
            "<input type='text' class='form-control input-sm weather-input' maxlength='32' " +
            "placeholder='ex: Bordeaux' />" +
            "<button type='submit' class='weather-btn btn btn-primary btn-sm'>Search</button>" +
            "</div>" +
            "</div>");

        $(".weather-btn").click(function () {
            let this_widget = $(this).parentsUntil(".panel").parent();

            if ($(this_widget).find(".weather-input").val()) {
                let uri = "http://api.worldweatheronline.com/premium/v1/weather.ashx?key=5087dcc1c04f4b40ad1200824171503&format=json&&fx=no&mca=no&show_comments=no&q="
                    + $(this_widget).find(".weather-input").val();

                $.get(uri, function (r) {
                    if (r.data.error) {

                    } else {
                        let city = r.data.request[0].query;
                        let temp_celsius = r.data.current_condition[0].temp_C;
                        let image = r.data.current_condition[0].weatherIconUrl[0].value;

                        $(this_widget).replaceWith("<div class='panel panel-primary'>" +
                            "<div class='panel-heading'>Weather" +
                            "<button class='fa fa-times delete_cross' aria-hidden='true'></button></div>" +
                            "<div class='panel-body weather_widget'>" +
                            "<div class='weather_widget'>" +
                            "<div class='row weather-info'>" +
                            "<h2 class='temp'>" + temp_celsius + "°</h2></span>" +
                            "<p class='city_and_country'>" + city + "</p>" +
                            "<img class='weather_icon' src='" + image + "' alt='" + city + "_icon'>" +
                            "</div>" +
                            "</div>" +
                            "</div>");
                    }
                }, "jsonp");
            }
        });

        $(".weather-input").keypress(function (e) {
            if (e.keyCode == 13)
                $(".weather-btn").trigger("click");
        });
    }
}
