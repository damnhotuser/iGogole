
class DateTimeWidget extends Widget {

    constructor() {
        super();

        this.monthnames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"];

        this.timestamp = new Date();

        this.hours = this.timestamp.getHours().toString();
        this.hours = this.hours.length < 2 ? "0" + this.hours : this.hours;
        this.minutes = this.timestamp.getMinutes().toString();
        this.minutes = this.minutes.length < 2 ? "0" + this.minutes : this.minutes;
        this.seconds = this.timestamp.getSeconds().toString();
        this.seconds = this.seconds.length < 2 ? "0" + this.seconds : this.seconds;

        this.day = this.timestamp.getDate();
        this.month = this.monthnames[this.timestamp.getMonth()];
        this.year = this.timestamp.getFullYear();
    }

    apply_to(parent) {

        parent.append("<div class='panel panel-primary'>" +
            "<div class='panel-heading'>Date & Time" +
            "<button class='fa fa-times delete_cross' aria-hidden='true'></button></div>" +
            "<div class='panel-body datetime_widget'>" +
            "<div class='time'>" +
            "<h1 class='time_display'>" + this.hours +
            ":" + this.minutes +
            ":" + this.seconds + "</h1>" +
            "</div>" +
            "<div class='date'>" +
            "<h3 class='date_display'>" + this.day + " of " +
            " " + this.month +
            " " + this.year +
            "</h3>" +
            "</div>" +
            "</div>" +
            "</div>");
    }

    actualise() {
        window.setInterval(function () {

            this.timestamp = new Date();
            this.monthnames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"];

            let ancient = this.hours;

            this.hours = this.timestamp.getHours().toString();
            this.hours = this.hours.length < 2 ? "0" + this.hours : this.hours;
            this.minutes = this.timestamp.getMinutes().toString();
            this.minutes = this.minutes.length < 2 ? "0" + this.minutes : this.minutes;
            this.seconds = this.timestamp.getSeconds().toString();
            this.seconds = this.seconds.length < 2 ? "0" + this.seconds : this.seconds;

            if (ancient != this.hours) {
                this.day = this.timestamp.getDate();
                this.month = this.monthnames[this.timestamp.getMonth()];
                this.year = this.timestamp.getFullYear();

                $(".date_display").text(this.day + " of " +
                    " " + this.month +
                    " " + this.year);
            }

            $(".time_display").text(this.hours +
                ":" + this.minutes +
                ":" + this.seconds);

        }, 1000);
    }
}