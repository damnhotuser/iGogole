/**
 * Created by damnhotuser on 16/03/17.
 */

class YoutubeWidget extends Widget {

    constructor(name) {
        super();
        if (name === undefined)
            this.video_name = "Banana boat song";
        else
            this.video_name = name;

    }

    apply_to(parent){
    parent.append("<div class='panel panel-primary'>" +
        "<div class='panel-heading'>Youtube video" +
        "<button class='fa fa-times delete_cross' aria-hidden='true'></button></div>" +
        "<div class='panel-body youtube_widget'>" +
        "<div class='container'>" +
        "<div class='row'>" +
        "<div class='youtube-search'>" +
        "<input type='text' class='form-control input-sm youtube-input' maxlength='64' placeholder='Search' />" +
        "<button type='button' class='youtube-btn btn btn-primary btn-sm'>Search</button>" +
        "</div>" +
        "</div>" +
        "</div>" +
        "<div class='youtube-div'></div>" +
        "<iframe class='yt-frame' width='330' height='400' src='http://www.youtube.com/embed?listType=search&list=" +
        this.video_name + "' frameborder='0' allowfullscreen/>" +
        "</div>" +
        "</div>");

        $(".youtube-btn").click(function () {
            let this_widget = $(this).parentsUntil(".panel").parent();
            if ($(this_widget).find(".youtube-input").val()) {
                $(this_widget).find('.yt-frame').remove();
                $(this_widget.find(".youtube-div")).replaceWith("<div class='youtube-div'></div>" +
                    "<iframe class='yt-frame' width='330' height='400' src='http://www.youtube.com/embed?listType=search&list=" +
                    $(this_widget).find(".youtube-input").val() + "' frameborder='0' allowfullscreen/>" +
                    "</div>");
                $(this_widget).find(".youtube-input").val("");
            }
        });

        $(".youtube-input").keypress(function (e) {
            if (e.keyCode == 13) {
                $(".youtube-btn").trigger("click");
            }
        });
    }
}