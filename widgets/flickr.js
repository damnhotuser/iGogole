/**
 * Created by damnhotuser on 16/03/17.
 */

class FlickrWidget extends Widget {

    //key=8e2daf518c9c65e317d26b8ccc0e713c
    //5c28ffd54e83a940

    //https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=8e2daf518c9c65e317d26b8ccc0e713c&tags=flower&per_page=1

    constructor() {
        super();
    }

    apply_to(parent) {

        let url = "";

        $.ajax({
            url: "https://api.flickr.com/services/rest/",
            type: 'get',
            async: false,
            data: {
                'tags' : 'dog',
                'format': 'json',
                'api_key': '8e2daf518c9c65e317d26b8ccc0e713c',
                'method': 'flickr.photos.search',
                'per_page' : '1'
            },
            dataType: 'jsonp',
            jsonpCallback: 'jsonFlickrApi',
            success: function (data) {
                let farm = data.photos.photo[0].farm;
                let id = data.photos.photo[0].id;
                let server = data.photos.photo[0].server;
                let secret = data.photos.photo[0].secret;

                url = "http://farm"+farm+".staticflickr.com/"+server+"/"+id+"_"+secret+".jpg";

                parent.append("<div class='panel panel-primary'>" +
                    "<div class='panel-heading'>Flickr" +
                    "<button class='fa fa-times delete_cross' aria-hidden='true'></button></div>" +
                    "<div class='row flickr-search'>" +
                    "<input type='text' class='form-control input-sm flickr-input' maxlength='64' placeholder='ex: doggy...' />" +
                    "<button type='button' class='flickr-btn btn btn-primary btn-sm'>Search</button>" +
                    "</div>" +
                    "<div class='panel-body flickr_widget'>" +
                    "<img class='flickr-photo' src='" + url + "'/>"+
                    "</div>");

                $(".flickr-btn").click(function () {
                    let this_widget = $(this).parentsUntil(".panel").parent();
                    if ($(this_widget).find(".flickr-input").val()) {
                        console.log($(this_widget).find(".flickr-input").val());
                        $.ajax({
                            url: "https://api.flickr.com/services/rest/",
                            type: 'get',
                            async: false,
                            data: {
                                'tags': $(this_widget).find(".flickr-input").val(),
                                'format': 'json',
                                'api_key': '8e2daf518c9c65e317d26b8ccc0e713c',
                                'method': 'flickr.photos.search',
                                'per_page': '1'
                            },
                            dataType: 'jsonp',
                            jsonpCallback: 'jsonFlickrApi',
                            success: function (data) {
                                let farm = data.photos.photo[0].farm;
                                let id = data.photos.photo[0].id;
                                let server = data.photos.photo[0].server;
                                let secret = data.photos.photo[0].secret;

                                let url = "http://farm" + farm + ".staticflickr.com/" + server + "/" + id + "_" + secret + ".jpg";

                                $(this_widget).find(".flickr-photo").replaceWith("<img class='flickr-photo' src='" + url + "'/>");
                            }
                        });
                    }
                });

                $(".flickr-input").keypress(function (e) {
                    if (e.keyCode == 13) {
                        $(".flickr-btn").trigger("click");
                    }
                });
            }
        });



    }
}