
class FootWidget extends Widget {

    constructor() {
        super();
    }

    apply_to(parent) {


        parent.append(
            "<div class='panel panel-primary'>" +
            "<div class='panel-heading'>Today's Fixtures" +
            "<button class='fa fa-times delete_cross' aria-hidden='true'></button>" +
            "</div>" +
            "<div class='panel-body foot_widget'>" +
            "<div class='container'>" +
            "<div class='row fixtures'>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "</div>"
        );

        $.ajax({
            headers: {'X-Auth-Token': 'ea53f2976ab94ef8b26715ace24231c6'},
            url: 'http://api.football-data.org/v1/fixtures?timeFrame=n1',
            dataType: 'json',
            type: 'GET',
        }).done(function (response) {

                for (let i = 0; i < response.fixtures.length; i++) {
                    let urlHome = response.fixtures[i]._links.homeTeam.href;
                    let imgHome = "";
                    $.ajax({
                        headers: {'X-Auth-Token': 'ea53f2976ab94ef8b26715ace24231c6'},
                        url: urlHome,
                        dataType: 'json',
                        type: 'GET',
                    }).done(function (repHome) {
                        imgHome = repHome.crestUrl;
                    });

                    let homeTeam = response.fixtures[i].homeTeamName;
                    let awayTeam = response.fixtures[i].awayTeamName;
                    let d = new Date(response.fixtures[i].date).toTimeString().split(" ")[0].split(":00")[0];
                    let goalHome = response.fixtures[i].result.goalsHomeTeam;
                    let goalAway = response.fixtures[i].result.goalsAwayTeam;
                    let result;
                    if (goalHome == null || goalAway == null) {
                        result = d;
                    }
                    else {
                        result = goalHome + "-" + goalAway;
                    }


                    let urlAway = response.fixtures[i]._links.awayTeam.href;
                    $.ajax({
                        headers: {'X-Auth-Token': 'ea53f2976ab94ef8b26715ace24231c6'},
                        url: urlAway,
                        dataType: 'json',
                        type: 'GET',
                    }).done(function (repAway) {
                        let imgAway = repAway.crestUrl;
                        $(".fixtures").append("<div class='fixture row'>" +
                            "<div class='home-team col-md-4'>" +
                            "<img class='img-home' height='50px' width='50px' src='" + imgHome + "'>" +
                            "<h1 class='teamname-home'>" + homeTeam + "</h1>" +
                            "</div>" +
                            "<h2 class='fixture-score col-md-4'>" + result + "</h2>" +
                            "<div class='away-team col-md-4'>" +
                            "<h1 class='teamname-away'>" + awayTeam + "</h1>" +
                            "<img class='img-away' height='50px' width='50px' src='" + imgAway + "'>" +
                            "</div>" +
                            "</div>" +
                            "<hr>"
                        );
                    });
                }
            }
        );
    }
}