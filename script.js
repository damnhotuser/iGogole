let column_to_add = 0;

$(document).ready(function () {

    console.log(window.location.pathname);

    // ADD WIDGET ROW ==================================

    let add_widget_button = $(".add_widget_button");
    let click_to_add_label = $("#click-to-add-label");

    click_to_add_label.hide().fadeIn(1500);

    $(".column").sortable({
        connectWith: ".column",
        dropOnEmpty: true,
        revert: "invalid",
        start: function (e, ui) {
            $(ui.placeholder).hide(300);
        },
        change: function (e, ui) {
            $(ui.placeholder).hide().show(300);
        }
    });

    // SEARCH BAR ======================================

    let search_bar = $("#search_bar");
    let search_button = $("#search_button");
    let search_list = $("#list");

    search_bar.keyup(function () {
        search_list.show();
        $.ajax({
            url: "http://suggestqueries.google.com/complete/search",
            dataType: "jsonp",
            data: {
                client: "chrome",
                q: search_bar.val()
            }
        }).done(function (data) {
            search_list.empty();
            var list = data[1];
            $(list).each(function (i) {
                var entry = this;

                $("<li role='presentation'>")
                    .text(entry)
                    .appendTo(search_list)
                    .data("query", entry).click(function () {
                    openInGoogle($(this).data("query"));
                });
            });
        });
        if (search_list.children().length == 0 || !search_bar.val().match(/\S/))
            search_list.hide();
    });

    search_button.click(function () {
        openInGoogle(search_bar.val());
    });
    search_bar.keydown(function (e) {
        if (e.keyCode === 13) {
            openInGoogle(search_bar.val());
        }
    });

    $(document).click(function (e) {
        if (document.activeElement != document.getElementById("search-section"))
            search_list.hide();
    })

    // =================================================
    // SIDEBAR =========================================

    let first_column = $("#first_column_widget");
    let second_column = $("#second_column_widget");
    let third_column = $("#third_column_widget");

    let columns = [first_column, second_column, third_column];

    $("#av-datetime-btn").click(function () {
        let widget = new DateTimeWidget();
        widget.apply_to(columns[column_to_add])
        widget.actualise();
    });
    $("#av-twitter-btn").click(function () {
        let widget = new TwitterWidget();
        widget.apply_to(columns[column_to_add])
        widget.actualise();
    });
    $("#av-foot-btn").click(function () {
        let widget = new FootWidget();
        widget.apply_to(columns[column_to_add])
    });
    $("#av-weather-btn").click(function () {
        let widget = new WeatherWidget();
        widget.apply_to(columns[column_to_add])
    });
    $("#av-flickr-btn").click(function () {
        let widget = new FlickrWidget();
        widget.apply_to(columns[column_to_add]);
    });
    $("#av-youtube-btn").click(function () {
        let widget = new YoutubeWidget();
        widget.apply_to(columns[column_to_add])
    });
    $("#av-gmaps-btn").click(function () {
        let widget = new GMapsWidget();
        widget.apply_to(columns[column_to_add])
    });

    add_widget_button.click(function () {
        click_to_add_label.stop().hide();
        column_to_add++;
        column_to_add %= 3;

        $('.delete_cross').click(function () {
            $(this).parent().parent().replaceWith("");
            column_to_add--;
            if (column_to_add == -1)
                column_to_add = 2;
        });
    });

    $('#clear_button').click(function () {
        $(".column").empty();
        column_to_add = 0;
        click_to_add_label.stop().fadeIn(1500);
    });
});

// UTILS ========================================

function openInGoogle(query) {
    window.open("http://www.google.com/#q=" + encodeURIComponent(query), "blank");
}


